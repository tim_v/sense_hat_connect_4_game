import math
import random
import copy
from sense_hat import SenseHat
from time import sleep
import pygame
from pygame.locals import *

pygame.init()
pygame.display.set_mode((640, 480))
sense = SenseHat()

# Returns a product of list.
def prod(list):
    product = 1
    for item in list:
        product *= item
    return product

# Creates a list of pascal's numbers.
def pascals_triangle(rows):
    for rownum in range (rows):
        newValue=1
        PrintingList = [newValue]
        for iteration in range (rownum):
            newValue = newValue * ( rownum-iteration ) * 1 / ( iteration + 1 )
            PrintingList.append(int(newValue))
    return PrintingList

# Return "1" or "2".
def coinflip():
    coin = math.floor(random.random()*2+1)
    return coin

# Prints line with player simbols "X" and "O" with "-" for free box.
def print_line(listX) :
    line = ""
    for item in listX :
        sign = ""
        if item == 1 :
            sign = "X"
        elif item == 2 :
            sign = "O"
        else :
            sign = "-"
        line += " " + str(sign)
    print(line)

# Prints numbers corresponding to the board and pointers over non empty rows.
def board_enumeration() :
    numbers = ""
    pointers = ""
    count = num_of_rows
    while count > 0 :
        numbers += " %s" % (num_of_rows - count + 1)
        if board[0][num_of_rows - count] != 0 :
            pointers += "  " # Two spaces.
        else:
            pointers += " ˇ" # Space and pointer.
        count -= 1
    print("\n" + numbers, "\n" + pointers)

# Prints board.
def print_board(the_board) :
    board_enumeration()
    for line in the_board :
        print_line(line)
    print()

# Checks if board is ful.
def board_full ():
    for item in board[0] :
        if item == 0 :
            return False
    return True

# Check if row is full.
def check_if_row_full (the_board, row) :
    if the_board[0][row - 1] == 0 :
        #print("prazno")
        return False
    else :
        #print("polno")
        return True

# Checks if there is winning position inside any of the lines.
def line_win(the_board) :
    total_variations_in_line = num_of_rows - inRow + 1 # Number of posible wining positions.
    for line in the_board :        
        variations_left = total_variations_in_line
        if total_variations_in_line < 1 :
            return False
        while variations_left > 0 :            
            start_position = total_variations_in_line - variations_left
            end_position = start_position + inRow
            if (prod(line[ start_position : end_position ])) == 1 :
                return True
            if (prod(line[ start_position : end_position ])) == math.pow(2, inRow) :
                return True
            variations_left -= 1
    return False

# Checks if there is winning position inside any of the rows.
def row_win(the_board) :    
    total_variations_in_row = num_of_lines - inRow + 1 # Calculates number of posible winning combinations in row.
    if total_variations_in_row < 1 :
        return False
    for r in range(num_of_rows) :
        variations_to_check = total_variations_in_row 
        for v in range(total_variations_in_row) :
            product_row = 1
            for i in range(inRow) :
                product_row *= the_board[i + total_variations_in_row - variations_to_check][r]
            variations_to_check -= 1 
            if (product_row == 1) or (product_row == math.pow(2, inRow)) :
                return True
    return False

# Checks if there is winning position diagnonally.
def side_win(the_board) :
    var_in_row = num_of_lines - inRow + 1 # 3
    var_in_line = num_of_rows - inRow + 1 # 4
    if num_of_rows < inRow or num_of_lines < inRow :
        return False
    for r_w in range(var_in_line) : # 4x
        var_to_check = var_in_row        
        for v_w in range(var_in_row) : # 3x
            product_side_NW = 1
            product_side_NE = 1
            for i_w in range(inRow) :
                product_side_NW *= the_board[v_w + i_w][r_w + i_w]
                product_side_NE *= the_board[v_w + i_w][inRow - 1 + r_w - i_w]
            if product_side_NW == 1 or product_side_NE == 1 :
                return True
            if product_side_NW == math.pow(2, inRow) or product_side_NE == math.pow(2, inRow) :
                return True
    return False

# Checks if there is winning position.
def win(the_board) :
    if line_win(the_board) or row_win(the_board) or side_win(the_board) == True :
        return True
    else :
        return False

# Generate board.
def board_generator(lines, rows):
    while lines > 0 :
        board.append( [ 0 for x in range(rows) ] )
        lines -= 1

# Puts value in row.
def put_in(the_board, pos, value) :
    for line in range(num_of_lines) :
        if the_board[num_of_lines -line - 1][pos - 1] == 0 :
            the_board[num_of_lines - line - 1][pos - 1] = value
            if the_board == board:
                animation(pos - 1, line, value)
            break

# Prompt player1 to make a move.
def player1_move():
    while True:
        num = select_row()
        if check_if_row_full(board, num):
            print("Row number", num, "is full. Please choose another row...\n")
        else:
            put_in(board, num, 1)
            return False

# Player 2 makes a random possible move unless it can prevent immediate defeat.
def player2_random():
    bot_options = pascals_triangle(num_of_rows)
    for i in range(num_of_rows):
        if check_if_row_full (board, i + 1) != 0:
            bot_options[i] = 0
    if 1 in safe_rows:
        bot_opt_after_2_level = copy.deepcopy(bot_options)
        for n in range(num_of_rows):
            if safe_rows[n] == 1:
                bot_opt_after_2_level[n] = 0
        if sum(bot_opt_after_2_level) != 0:
            bot_options = bot_opt_after_2_level
    rand_num  = math.floor(random.random()*sum(bot_options)+1)
    total = 0
    for z in range(num_of_rows):
        total += bot_options[z]
        if total >= rand_num:
            print(player2, "played row:", (z + 1))
            put_in(board, z + 1, 2)
            return True  

#Checks if player2 can win.
def player2_check_win_possibility():
    test_board = copy.deepcopy(board)
    for i in range(num_of_rows):
        if check_if_row_full(test_board, i + 1):
            continue
        put_in(test_board, i + 1, 2)
        if win(test_board):
            print(player2, "played row:", (i + 1))
            put_in(board, i + 1, 2)
            return True
        test_board = copy.deepcopy(board)
    return False

#Checks if player1 can win.
def player2_check_lose_possibility():
    test_board = copy.deepcopy(board)
    for i in range(num_of_rows):
        if check_if_row_full(test_board, i + 1):
            continue
        put_in(test_board, i + 1, 1)
        if win(test_board):
            print(player2, "played row:", (i + 1))
            put_in(board, i + 1, 2)
            return True
        test_board = copy.deepcopy(board)
    return False

# Checks if player2 move can give player1 a winning move.
def player2_check_second_level():    
    if num_of_lines < 2:
        return False
    test_board = copy.deepcopy(board)
    for i in range(num_of_rows):
        if test_board[0][i] != 0:
            continue
        put_in(test_board, i + 1, 2)
        put_in(test_board, i + 1, 1)
        if win(test_board):
            safe_rows[i] = 1
        test_board = copy.deepcopy(board)

# Make a move as player2.
def player2_move():
    if player2_check_win_possibility():
        return None
    if player2_check_lose_possibility():
        return None
    global safe_rows
    safe_rows = list(0 for x in range(num_of_rows))
    player2_check_second_level()
    player2_random()
    safe_rows = list(0 for x in range(num_of_rows))

# Alternately calls player1 and play as player2.
def gameflow() :
    screen_generator()
    while board_full() is False:
        player1_move()
        print_board(board)
        screen_generator()
        if win(board):
            print("You won!")
            return None
        player2_move()
        print_board(board)
        screen_generator()
        if win(board):
            print("You lost! Player", player2, "has won.")
            return None
    print("The game is a draw.")
    return None

# Beginnig function
def start():
    if coinflip() == 2:
        print("\n" + player2 + " has won the imaginary coinflip. So it will start first.\n")
        player2_random()
    else:
        print("\nYou won imaginary coinflip! You start first.\n")
    print_board(board)
    gameflow()
    screen_blink(6)
    sleep(3)
    sense.clear()

"""SenseHat part"""
# Makes screen blink n-times.
def screen_blink(blinks):
    for n in range(blinks):
        add_color(e, e)
        sense.set_pixels(game_screen)
        sleep(0.2)
        screen_generator()
        sleep(0.2)

# Generates screen on SenseHat
def screen_generator():
    add_color(r, b)
    sense.set_pixels(game_screen)

def add_color(p1, p2):
    for line in range(6):
        for row in range(7):
            if board[ line ][ row ] == 1 :
                game_screen[ ( 2 + line ) * 8 + row  ] = p1
            if board[ line ][ row ] == 2 :
                game_screen[ ( 2 + line ) * 8 + row  ] = p2

def animation(row, line, value):
    color = e
    print(row, line, value)
    if value == 1:
        color = r
    elif value == 2:
        color = b
    for pixel in range(8 - line):
        sense.set_pixel(row, 1, g)
        sense.set_pixel(row, pixel, color)
        sleep(0.025)
        sense.set_pixel(row, pixel, e)
        sleep(0.025)

def select_row():
    global player1_position
    try:
        player1_position
    except:
        player1_position = 3
    while True:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                sense.set_pixel(player1_position, 0, 0, 0, 0)  # Black 0,0,0 means OFF
                if event.key == K_DOWN:
                    return (player1_position + 1)
                elif event.key == K_RIGHT and player1_position < 6:
                    player1_position += 1
                elif event.key == K_LEFT and player1_position > 0:
                    player1_position -= 1
            sense.set_pixel(player1_position, 0, 255, 0, 0)

r = (255, 0, 0) # Red
b = (0, 0, 255) # Blue
g = (100, 100, 0) # Grey
e = (0, 0, 0) # Empty
board = [[0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 2, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [0, 0, 2, 2, 1, 0, 0]]

global game_screen
game_screen = [
e, e, e, e, e, e, e, g,
g, g, g, g, g, g, g, g,
e, e, e, e, e, e, e, g,
e, e, e, e, e, e, e, g,
e, e, e, e, e, e, e, g,
e, e, e, e, e, e, e, g,
e, e, e, e, e, e, e, g,
e, e, e, e, e, e, e, g,
]

num_of_rows = 7
num_of_lines = 6
inRow = 4 # Must be smaller or equal than both num_of_rows and num_of_lines.
board = list()
game_over = False
player1 = "Player1"
player2 = "Ultra IQ Level Over 9000 Super Computer"
safe_rows = list(0 for x in range(num_of_rows))
board_generator(num_of_lines, num_of_rows) # Creates empty board.
start()

